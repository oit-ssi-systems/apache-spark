def vis_line( df="df", x="d_time_window", ylist="count", by=None, gtype="line", marker="o", theme="light"):
    """take a dataframe and create a line plot
    
    Parameters
    ----------
    df: dataframe imported using %spark -o <df> -n <count>
        required field
        Can be used with local pandas dataframes
    x: string that is the name of one of the columns in df
        required
        If omitted, only one box is plotted
    ylist: string or list of strings. Strings must be the name(s) of one of the columns in df
        required
    by: string that is the name of one of the columns in df
        optional
        if specified, the values of by will be used to split the line into multiple lines
    marker:
        the matplotlib marker indicator.  Ex: "o", "x", "."
    theme: ["light", "dark"]
        optional
        If "dark", the plot is modified for dark themed jupyter instances
    """

    if theme == "dark":
        from jupyterthemes import jtplot
        jtplot.style(theme='onedork')

    import matplotlib.pyplot as plt
    plt.rcParams['figure.figsize'] = [35, 10]

    import pandas as pd
    if pd.core.dtypes.common.is_datetime_or_timedelta_dtype(df[x]):
        import matplotlib.dates as mpld
        import pytz
        ax = plt.gca()
        frmt = mpld.DateFormatter('%Y-%m-%d %H:%M:%S')
        frmt.set_tzinfo( pytz.timezone("US/Eastern") )
        ax.xaxis.set_major_formatter( frmt )
        #ax.xaxis.set_major_formatter( mpld.DateFormatter('%Y-%m-%d %H:%M:%S') )
        plt.xticks( rotation=45 )
        
    plt.ticklabel_format( axis="y", style="plain", useLocale=False )

    if isinstance( ylist, str ):
        ylist = [ ylist ]

    df = df.sort_values(x)

    gargs = {}
    if gtype == "scatter":
        pltr = plt.scatter
    else:
        pltr = plt.plot
        gargs["markersize"] = 12

    if by != None:
        for b in df[by].unique():
            df1 = df[ df[by] == b ]
            for y in ylist:
                pltr( df1[x], df1[y], label=str(b) + " - " + str(y), marker=marker, **gargs )
    else:
        for y in ylist:
            pltr( df[x], df[y], label=y, marker=marker, **gargs )
    plt.legend()
    plt.show()


def vis_box( df, x="", y="count", theme="light" ):
    """takes a dataframe and creates a box and whisker plot
    
    Parameters
    ----------
    df: dataframe imported using %spark -o <df> -n <count>
        required field
        Can be used with local pandas dataframes
    x: string that is the name of one of the columns in df
        optional
        If omitted, only one box is plotted
    y: string that is the name of one of the columns in df
        required
    theme: ["light", "dark"]
        optional
        If "dark", the plot is modified for dark themed jupyter instances
    
    Returns
    --------
    Nothing
    
    Output
    -------
    Box and whisker plot
    """
    import seaborn as sb
    import matplotlib.pyplot as plt
    plt.rcParams['figure.figsize'] = [35, 10]

    import matplotlib.dates as mpld
    import pytz
    ax = plt.gca()
    frmt = mpld.DateFormatter('%Y-%m-%d %H:%M:%S')
    frmt.set_tzinfo( pytz.timezone("US/Eastern") )
    ax.xaxis.set_major_formatter( frmt )
    #ax.xaxis.set_major_formatter( mpld.DateFormatter('%Y-%m-%d %H:%M:%S') )
    plt.xticks( rotation=45 )    
    plt.ticklabel_format( axis="y", style="plain", useLocale=False )

    if x != "":
        ax = sb.boxplot( x=x, y=y, data=df )
    else:
        ax = sb.boxplot( y=y, data=df )
    if theme == "dark":
        plt.setp( ax.lines, color="w" )