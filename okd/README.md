# Jupyter/Spark/Etc. magic in OKD

The intent of this document is to provide steps to setup a template which will take a custom Jupyter image & a user-provided notebook, and create a buildConfig to generate an image, then run the image on-demand or via cron.

This is effectively two projects - one to build and maintain the custom image with source-to-image scripts installed, and a second to build and maintain template files that will allow *users* to run their notebooks.

## Prerequesites

1.  We must have an image with source-to-image scripts to use in the templated buildConfig


## Creating and maintaining our source-to-image image

*Process:*

1.  Create an ImageStream to track the upstream image
2.  Create a BuildConfig to build/rebuild our own image and push it to the registry

### Create an ImageStream to track the upstream image

We will create an ImageStream that has a 'registry.cloud.duke.edu' path, but effectively points directly to DockerHub.  This will allow our builds to use the image, but OKD will continually poll for updates.

The ImageStream object is tiny:

```
apiVersion: "v1"
kind: "ImageStream"
metadata:
  name: "all-spark-notebook"
spec:
  dockerImageRepository: "docker.io/jupyter/all-spark-notebook"
```

This tells OKD to track the image upstream, but creates an ImageStream named 'all-spark-notebook' for us to use in our Docker images and projects.

**Create the ImageStream from the file:**

A sample/example image stream is included in the `okd` directory of this repo.  Modify it to suit if necessary, then create the ImageStream with the `oc create -f <fileanme>` command:

`oc create -f okd/all-spark-notebook_imagestream.yaml`

Within a few minutes, OKD will poll Docker.io, and import all the tags for the all-spark-notebook image:

```
oc describe is all-spark-notebook
Name:			all-spark-notebook
Namespace:		sparktest
Created:		2 minutes ago
Labels:			<none>
Annotations:		openshift.io/image.dockerRepositoryCheck=2019-01-24T16:14:06Z
Docker Pull Spec:	docker-registry.default.svc:5000/sparktest/all-spark-notebook
Image Lookup:		local=false
Unique Images:		50
Tags:			50

latest
  tagged from docker.io/jupyter/all-spark-notebook:latest

  * docker.io/jupyter/all-spark-notebook@sha256:dd1a71707e1ebb2523a501c5ee3534cd99837cc893803e2d8ad404dcb5feaa4d
      About a minute ago

4.0
  tagged from docker.io/jupyter/all-spark-notebook:4.0

  * docker.io/jupyter/all-spark-notebook@sha256:624bdc46b678e384feba23e5f814312d4c6fb7ecdc40c8fc55d6fb73d42ac052
      About a minute ago
```

### Create an ImageStream for our own builder image

In order for our BuildConfig, below, to have a place to push the image it builds, we need to create an empty ImageStream for it to use:

```
apiVersion: image.openshift.io/v1
kind: ImageStream
metadata:
  name: jupyter-notebook-builder
spec:
  lookupPolicy:
    local: false
```

**Create the ImageStream from the file:**

A sample/example image stream is included in the `okd` directory of this repo, and can probably be used as-is. Create the ImageStream with the `oc create -f <fileanme>` command:

`oc create -f okd/jupyter-notebook-builder_imagestream.yaml`


### Create a BuildConfig to build/rebuild our own image

In the repo, we have the Dockerfile which will be used to build our image.  OKD uses buildConfigs to describe a build process, and we can use the buildConfig to manage the image for us, using the [Docker Strategy](https://docs.okd.io/latest/dev_guide/builds/build_strategies.html#docker-strategy-options).

BuildConfigs are not complicated.  There's really only two important parts: the build type, and the source of data.  This BuildConfig will be a Docker Strategy-type (ie: build a Docker image from a Dockerfile we provide), and the data source will be our Git repo.

*Note:* A only tricky part of the Docker Strategy build is that the `FROM` instruction in the Dockerfile is replaced by the `from` of the BuildConfig.  In this case, we'll reference the ImageStream we just created, instead.

The BuildConfig Type:

```
strategy:
  type: Docker
  dockerStrategy:
    from:
      kind: ImageStreamTag
      name: all-spark-notebook:latest
```

The above tells OKD to build a Docker image, and as noted above, use the "from" section here to tell it the parent image.

The BuildConfig Source:

```
  source:
    type: Git
    git:
      ref: master
      uri: https://gitlab.oit.duke.edu/clc0317/apache-spark.git
```

The above tells OKD to use our git repo (in this case my fork - substitute your own).

Finally, a small section to name the resulting image and tell OKD to push it to the ImageStream:

```
output:
  to:
    kind: ImageStreamTag
    name: jupyter-notebook-builder:1.0
```

**Modify the BuildConfig and create it from the file:**

A sample/example BuildConfig is included in the `okd` directory of this repo.  Modify it to include your source repository, then create the BuildConfig with the `oc create -f <fileanme>` command:

`oc create -f okd/jupyter-notebook-builder_buildconfig.yaml`


### Build the BuilderImage

Now that all the pieces are in place to create your builder image, start a new build manually:

```
oc start-build jupyter-notebook-builder --follow
```

You can follow along with the command `oc logs -f <buildname>`:

```
oc logs -f build.build.openshift.io/jupyter-notebook-builder-2
Cloning "https://gitlab.oit.duke.edu/clc0317/apache-spark.git" ...
	Commit:	d5cf8e7c112f64efdf3eab1be8d2268a95d5acf7 (adding helpers; updated dockerfile)
	Author:	Christopher Collins <collins.christopher@gmail.com>
	Date:	Thu Jan 24 11:57:09 2019 -0500
Replaced Dockerfile FROM image jupyter/all-spark-notebook:latest

<snip>
```

When the build completes, we now have a source-to-image image we can use to build custom notebook runners!


## Test the source-to-image builder image

A throwaway ImageStream, BuildConfig and Pod can be used to test the "jupyter-notebook-builder" image we just created, using the [Source-to-Image strategy](https://docs.okd.io/latest/dev_guide/builds/build_strategies.html#source-to-image-strategy-options) that regular users will use with the template we create later in this process.

A sample/example files for these are included in the `okd` directory of this repo.  Run the following to create them:


### Create a test ImageStream

We need an empty image stream for our test buildconfig to output to.

```
oc create -f test-builder_imagestream.yaml
```


### Create a test BuildConfig

Create the BuildConfig:

```
oc create -f test-builder_buildconfig.yaml
```

This sample build config uses *this* repo as the source, and the 'example-notebooks' directory as the contextdir.  In real life, these values would be the repo and contextdir where the users' notebooks are stored.


### Start a new build

Start a new build and follow the output:

```
oc start-build test-builder --wait --follow
build.build.openshift.io/test-builder-3 started
Cloning "https://gitlab.oit.duke.edu/clc0317/apache-spark.git" ...
 Commit:	6baf84afb2544fe1730cfe36035cb3da786038ea (removing CMD from builder image in favor of s2i)
 Author:	Christopher Collins <collins.christopher@gmail.com>
 Date:	Thu Jan 24 13:23:25 2019 -0500
Using docker-registry.default.svc:5000/sparktest/jupyter-notebook-builder@sha256:de7162abe6d438a0cca5c490e9d1630b788813d6cf51eb21eeb1552af5282bdf as the s2i builder image
---> Installing application source...
<snip>
```


### Create and validate a pod

Create the pod:

```
oc create -f test-builder_pod.yaml
```

After the pod is created, check the logs:

```
oc logs -f test-builder
Traceback (most recent call last):
  File "/home/jovyan/new-sparkmagic-build.py", line 24, in <module>
    scoped=os.environ['NETID']
  File "/opt/conda/lib/python3.6/os.py", line 669, in __getitem__
    raise KeyError(key) from None
KeyError: 'NETID'
```

Despite the Python error, this is working as expected.  The command specified in the source-to-image "run" script is being executed, in the homedir of the jovyan user.

You can use `oc debug test-builder` to poke around:

```
oc debug test-builder
Defaulting container name to test-builder.
Use 'oc describe pod/test-builder-debug -n sparktest' to see all of the containers in this pod.

Debugging with pod/test-builder-debug, original command: <image entrypoint>
Waiting for pod to start ...
If you don't see a command prompt, try pressing enter.

$ pwd
/home/jovyan
$ ls
config-template.json  new-sparkmagic-build.py  send-email.ipynb  write-csv.ipynb
Demo-from-Skip.ipynb  semail-10jan.ipynb       work
$ ls /usr/libexec/s2i
assemble  run  save-artifacts  usage
```

The script is there, as are the example notebooks from the "example-notebooks" contextdir mentioned above, and the source-to-image files are in the correct place.

### Cleanup the test objects

All of the test objects are tagged with a label `app=test-builder`:

```
oc get all -l app=test-builder
NAME               READY     STATUS             RESTARTS   AGE
pod/test-builder   0/1       CrashLoopBackOff   6          7m

NAME                                          TYPE      FROM         LATEST
buildconfig.build.openshift.io/test-builder   Source    Git@master   3

NAME                                      TYPE      FROM          STATUS     STARTED          DURATION
build.build.openshift.io/test-builder-1   Source    Git@6baf84a   Complete   14 minutes ago   11s
build.build.openshift.io/test-builder-2   Source    Git@6baf84a   Complete   14 minutes ago   25s
build.build.openshift.io/test-builder-3   Source    Git@6baf84a   Complete   13 minutes ago   15s
```

They can be deleted all at once using the label:

```
oc delete all -l app=test-builder
pod "test-builder" deleted
buildconfig.build.openshift.io "test-builder" deleted
imagestream.image.openshift.io "test-builder" deleted
```

Now that we've verified the builder image works to create source-to-image images, we can create templates that make use of it to do the same thing.
