#!/usr/bin/python3
# This script is part of the Openshift stream to image framework, in that it is used to plug in the user netID and ambari password to the sparkmagic config.json in order to build a pod with the correct credentials.
# The problem at the moment is that the passwords to connect to spark are created by spark on apache-spark-01 and put in an xml file, so until there is a way to get those out of there, they'll have to be added by hand somewhere. These passwords are stored in vault, though, so eventually we'll have to figure out if we can update the passwords on the fly - i.e. if you're running a job and your ambari pw changes, do you lose your connection? Or do you get to finish that job before you need to restart the kernel?

import hvac
import json
import os
import sys
import subprocess
import pwd
from pwd import getpwuid
from shutil import copyfile
import email
import smtplib
import argparse
#from email.message import EmailMessage


# In a future iteration of this script, maybe it can email the person who is attempting to run it.
##server=smtplib.SMTP('smtp.duke.edu',587)
#s=smtplib.SMTP('smtp.duke.edu')
#
#msg = EmailMessage()
#msg['From']='jupyterhub@duke.edu'
#msg['To']='bryn@duke.edu'
##msg['To']='bryn@duke.edu,mt73@duke.edu,eah33@duke.edu'
#msg['Subject']= 'this is the job that creates config.json for openshift'
#

magic_config='/Users/bryn/Dcuments/work/spark/workspace/config-template.json'

# we pass in the netID at run time, preferably for the person picking the job


try:
	myname=sys.argv[1]
except:
	myname=os.environ['USER']

print ("myname = %s") % (myname)

###### Vault snip 1 below here
def parse_args():
    """Just some simple argument parsing here
    """

    parser = argparse.ArgumentParser(
        description=(
          "Refresh of environment, including things like consul and vault "
          "tokens"
        )
    )
    default_identity_file = os.path.expanduser("~/.vault_role.yaml")
    print (default_identity_file)
    parser.add_argument('-i', '--vault-identity-file',
                        default=default_identity_file,
                        type=argparse.FileType('r'))
    parser.add_argument('-c', '--consul-mount', default='consul-sd',
                        help='Consul mount in vault')

    return parser.parse_args()

def get_approle_token(*args, **kwargs):
    identity_info = kwargs.get('identity_info')

    # Log in to the approle
    login = requests.post(
        "%s/v1/auth/approle/login" % identity_info['vault']['vault_addr'],
        json={
            'role_id': identity_info['vault']['role_id'],
            'secret_id': identity_info['vault']['secret_id']
        }
    )

    try:
        token = login.json()['auth']['client_token']
    except Exception:
        sys.stderr.write(
            "Could not get client token, please check your credentials\n")
        sys.exit(2)

    return token

#if os.path.exists(os.path.expanduser("~/.vault_role.yaml")):
#        identity_file = os.path.expanduser("~/.vault_role.yaml")

#accountsfile='/Users/bryn/Documents/work/spark/accounts.txt'
#vault = hvac.Client(url='https://vault-systems.oit.duke.edu',token=os.environ['VAULT_TOKEN'])
#v= open('/Users/bryn/.vault-role.yaml', 'r')
#for line in (v):
#	print (line)

#vault = hvac.Client(url='https://vault-systems.oit.duke.edu')
#vault.auth_approle(role_id, secret_id)

##### Vault snip 1 end

secret = 'secret/apache-spark/%s' % (myname)
print ("secret = %s") % (secret)
try:
	the_secret = (vault.read(secret))
#	print ("the_secret = %s") % (the_secret)
	pw = the_secret['data']
#	print (pw)
	actual = pw['password']
#	print ("%s = %s") % (myname,actual)
except:
	print ("account %s not in vault") % (myname)


with open('/Users/bryn/Documents/work/spark/workspace/config-template.json', 'r') as fp:
#	print ("fp = %s") % (fp)
	obj = json.load(fp)
	try:
#		print (obj)
		kpc = obj["kernel_python_credentials"]
#		print (kpc)
		ksc=obj["kernel_scala_credentials"]
		ksc["username"]=myname
		ksc["password"]=actual
#		print (ksc)
		kpc = obj["kernel_python_credentials"]
		kpc["username"]=myname
		kpc["password"]=actual
#		magicconfig="/home/%s/.sparkmagic/config.json" % (myname)
		magicconfig="/Users/bryn/Documents/work/dockers/openshift-jupyter-dev/config.json"
		session_configs=obj["session_configs"]
		session_configs["proxyUser"]=myname
		with open(magicconfig, 'w') as fp:
			json.dump(obj, fp, indent=4)
	except:
		print ("No ambari p/w in vault.")

